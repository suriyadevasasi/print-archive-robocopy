﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_Archive
{
    class Program
    {
        public static void Main()
            {

            string destinationsource = ConfigurationManager.AppSettings["DestinationSource"];
            string primarysource = ConfigurationManager.AppSettings["PrimarySource"];
            string parameters = ConfigurationManager.AppSettings["Parameters"];
            string logs = ConfigurationManager.AppSettings["Logs"];

            // Robocopy Command 
            //string Value1 = @"Robocopy \\SPRNMAINTA01\TestPrint \\SPRNMAINTA02\ReceivePrint\Test\ /copyall /LOG+:C:\ScriptLog\Logs.txt /TEE /dcopy:T";
                string Value1 = "Robocopy" + primarysource + destinationsource + parameters + logs;


            string root = @"C:\ScriptLog\";
            

                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.UseShellExecute = false;
                process.Start();

                // If directory does not exist, create it. 
                 if (!Directory.Exists(root))
                {
                Directory.CreateDirectory(root);
                 }


                process.StandardInput.WriteLine(Value1);
                process.StandardInput.Flush();
                process.StandardInput.Close();
                process.WaitForExit();
                Console.WriteLine(process.StandardOutput.ReadToEnd());
                Console.ReadKey();
            }
        }

    }

